import React, { Component } from 'react';
import { View, Text } from 'react-native';
import firebase from 'firebase';
import { Header } from './components/common';
import LoginForm from './components/LoginForm';

class App extends Component {
  componentWillMount() {
    firebase.initializeApp({
      apiKey: 'AIzaSyBJuMdDZ8_XZQoFfX9RBI-PqpjKgh0R6Ms',
      authDomain: 'auth-becb6.firebaseapp.com',
      databaseURL: 'https://auth-becb6.firebaseio.com',
      projectId: 'auth-becb6',
      storageBucket: 'auth-becb6.appspot.com',
      messagingSenderId: '44762954268'
    });
  }

  render() {
    return (
      <View>
        <Header headerText="Authentication" />
        <LoginForm />
      </View>
    );
  }
}

export default App;

Create a project with a specific version:
  $ react-native init myApp --version 0.55.4

See all devices:
  $ adb devices

Run app:
  $ react-native run-android

Run app in a specific device:
  $ react-native run-android --deviceId {ID}

Reload app on device:
  $ adb shell input text "RR"

Open Developer Menu on Android:
  $ adb shell input keyevent 82

Install Axios:
  $ npm install --save Axios

Install Babel dependencies:
  $ npm install @babel/core@7.0.0-beta.40 eslint@^3.17.0 || ^4.0.0

Corrigir erro ao "buildar" um novo projeto:
1. Criar arquivo "local.properties" em "projeto/android"
2. adicionar: "sdk.dir = C:\\Users\\USERNAME\\AppData\\Local\\Android\\sdk" ao arquivo
